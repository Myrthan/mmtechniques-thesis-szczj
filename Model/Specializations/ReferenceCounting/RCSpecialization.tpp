//
// Created by Szczepaniec, Jan  on 9/8/17
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "RCSpecialization.h"

template<typename T>
template<typename ...Params>
var<T, RCSimple>::var(Params &&... params) {
    void *mem_ptr = malloc(sizeof(T));
    T *t = new(mem_ptr) T(std::forward<Params>(params)...);
    body = new shared<T>(t);
}

template<typename T>
var<T, RCSimple>::var(const var &v): body(v.body) {
    body->retain();
}

template<typename T>
var<T, RCSimple>::var(var &v): var(static_cast<var const &>(v)) {
}

template<typename T>
var<T, RCSimple>::var(var &&v): body(v.body) {
    v.body = nullptr;
}

template<typename T>
var<T, RCSimple>::var(var<T, HeapAllocSBRM> &&v): body(new shared<T>(v.instance)) {
    v.instance = nullptr;
}

template<typename T>
var<T, RCSimple>::~var() {
    if (body) {
        body->release();
        body = nullptr;
    }
}

template<typename T>
T &var<T, RCSimple>::operator()() {
    return *(body->get());
}

template<typename T>
void var<T, RCSimple>::release() {
    if (body) {
        body->release();
        body = nullptr;
    }
}

template<typename T>
void *var<T, RCSimple>::operator new(std::size_t, void *p) {
    return p;
}
