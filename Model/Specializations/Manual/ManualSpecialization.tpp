//
// Created by Szczepaniec, Jan  on 9/8/17.
//

template<typename T>
template<typename ...Params>
var<T, Manual>::var(Params &&... params) {
    void *mem_ptr = malloc(sizeof(T));
    body = new(mem_ptr) T(std::forward<Params>(params)...);
}

template<typename T>
var<T, Manual>::var(var &v): body(v.body) {
    this->~var<T, Manual>();
};

template<typename T>
var<T, Manual>::~var() {
    body = nullptr;
}

template<typename T>
T &var<T, Manual>::operator()() const {
    return *body;
}

template<typename T>
void var<T, Manual>::freeTarget() {
    body->~T();
    free(body);
    body = nullptr;
}

template<typename T>
void *var<T, Manual>::operator new(std::size_t, void *p) {
    return p;
}

// params argument is default argument for initialization of every array element
template<typename T, std::size_t array_size>
var<T[array_size], Manual>::var(const T &params): body(malloc(sizeof(T) * array_size)) {
    T *array_ptr = static_cast<T *>(body);
    for (int i = 0; i < array_size; i++) {
        new(array_ptr + i) T(params);
    }
}

template<typename T, std::size_t array_size>
var<T[array_size], Manual>::var(std::initializer_list<T> elements): body(malloc(sizeof(T) * elements.size())) {
    using namespace boost::adaptors;
    T *array_ptr = static_cast<T *>(body);
    for (auto const &elem: elements | indexed(0)) {
        *(array_ptr + elem.index()) = elem.value();
    }
}

template<typename T, std::size_t array_size>
var<T[array_size], Manual>::var(var &v): body(v.body) {
};

template<typename T, std::size_t array_size>
var<T[array_size], Manual>::~var() {
    body = nullptr;
}

template<typename T, std::size_t array_size>
T &var<T[array_size], Manual>::operator()() const {
    return *(static_cast<T *>(body));
}

template<typename T, std::size_t array_size>
T var<T[array_size], Manual>::operator[](int i) const {
    return *(static_cast<T *>(body) + i);
}

template<typename T, std::size_t array_size>
T &var<T[array_size], Manual>::operator[](int i) {
    return *(static_cast<T *>(body) + i);
}

template<typename T, std::size_t array_size>
void *var<T[array_size], Manual>::operator new(std::size_t, void *p) {
    return p;
}

template<typename T, std::size_t array_size>
void var<T[array_size], Manual>::freeTarget() {
    T *array_ptr = static_cast<T *>(body);
    for (int i = array_size - 1; i >= 0; i--) {
        static_cast<T *>(array_ptr + i)->~T();
    }
    free(body);
    body = nullptr;
}