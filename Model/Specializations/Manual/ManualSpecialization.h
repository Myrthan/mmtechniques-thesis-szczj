//
// Created by Szczepaniec, Jan  on 9/12/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_MANUALSPECIALIZATION_H
#define MMTECHNIQUES_THESIS_SZCZJ_MANUALSPECIALIZATION_H

#include "../ModelSpecialization.h"
#include <boost/range/irange.hpp>
#include <boost/range/adaptor/indexed.hpp>

template<typename T>
class var<T, Manual> {
public:
    template<typename ...Params>
    var(Params &&... params);

    var(const var &v) = delete;

    var(var &v);

    var(var &&v) = delete;

    ~var();

    T &operator()() const;

    void freeTarget();

    static void *operator new(std::size_t, void *);

private:
    T *body;
};

template<typename T, std::size_t array_size>
class var<T[array_size], Manual> {
public:
    var(const T &);

    var(std::initializer_list<T>);

    var(const var &) = delete;

    var(var &v);

    var &operator=(var const &) = delete;

    ~var();

    T &operator()() const;

    T operator[](int) const;

    T &operator[](int);

    void freeTarget();

    // allow only var placement allocation
    static void *operator new(std::size_t, void *);

private:
    void *body;
};

#include "ManualSpecialization.tpp"

#endif


//MMTECHNIQUES_THESIS_SZCZJ_MANUALSPECIALIZATION_H
