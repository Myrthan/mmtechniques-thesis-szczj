//
// Created by Szczepaniec, Jan  on 11/27/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_FIELDSPECIALIZATION_H
#define MMTECHNIQUES_THESIS_SZCZJ_FIELDSPECIALIZATION_H

#include "../ModelSpecialization.h"
#include "../../Techniques/Struct/Field.h"

template<typename Struct, typename T, T Struct::* ptr, typename MM>
class var<T, Field<ptr, MM>> {
public:
    template<typename Pool, typename ...Params>
    var(Pool &, Params &&... params);

    var(const var &v) = delete;

    var(var &v) = delete;

    var(var &&v);

    ~var();

    T &operator()();

    static void *operator new(std::size_t, void *p) { return p; }

private:
    Struct *struct_pointer;
};

template<typename Struct, typename T, T Struct::* ptr>
class var<T, Field<ptr, RCSimple>> {
public:
    template<typename Pool, typename ...Params>
    var(var<Pool, RCSimple> &, Params &&... params);

    var(const var &v) = delete;

    var(var &v) = delete;

    var(var &&v);

    ~var();

    T &operator()();

    static void *operator new(std::size_t, void *p) { return p; }

private:
    var<Struct, RCSimple> struct_pointer;
};

#include "FieldSpecialization.tpp"

#endif //MMTECHNIQUES_THESIS_SZCZJ_FIELDSPECIALIZATION_H
