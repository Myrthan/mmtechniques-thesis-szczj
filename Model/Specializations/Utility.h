//
// Created by Szczepaniec, Jan on 14/09/2018.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_UTILITY_H
#define MMTECHNIQUES_THESIS_SZCZJ_UTILITY_H

#include "../Techniques/MemoryBlock.h"
#include "../Techniques/ReferenceCounting/RCSimple.h"
#include "../Techniques/Regions/Region.h"
#include "../Techniques/Regions/StaticRegion.h"
#include "../Techniques/Regions/DynamicRegion.h"

inline const long long localStackSize = 20000L;

class Stack {
protected:
    inline static MemoryBlock<localStackSize> stack;
};

template<typename T>
struct innermost_impl {
    using type = T;
};

template<typename RT>
struct innermost_impl<RegionTechnique<RT, RCSimple>> {
    using type = RT;
};

template<typename RT1, typename RT2, typename MM>
struct innermost_impl<RegionTechnique<RT1, RegionTechnique<RT2, MM>>> {
    using type = typename innermost_impl<RegionTechnique<RT2, MM>>::type;
};

template<typename T>
using innermost = typename innermost_impl<T>::type;

// sanity check
static_assert(std::is_same<innermost<RegionTechnique<StaticRegion<>,
        RegionTechnique<StaticRegion<>,
                RegionTechnique<DynamicRegion<>,
                        RegionTechnique<StaticRegion<>,
                                RCSimple>>> > >,
        StaticRegion<>>::value, "");

#endif //MMTECHNIQUES_THESIS_SZCZJ_UTILITY_H
