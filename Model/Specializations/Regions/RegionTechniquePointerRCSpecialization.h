//
// Created by Szczepaniec, Jan on 23/07/2018.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUEPOINTERRCSPECIALIZATION_H
#define MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUEPOINTERRCSPECIALIZATION_H

#include "../ModelSpecialization.h"

template<typename T, typename RT1, typename RT2>
class var<var<T, RegionTechnique<RT1, RCSimple>>, RegionTechnique<RT2, RCSimple> > {
public:
    var(var<RT2, RCSimple> &, var<T, RegionTechnique<RT1, RCSimple>> &);

    var(var &v);

    var(var &&v) = delete;

    ~var();

    var<T, RegionTechnique<RT1, RCSimple>> &operator()();

    static void *operator new(std::size_t, void *);

    var<RT2, RCSimple> external_region;
private:
    var<T, RegionTechnique<RT1, RCSimple>> *body;
};

#include "RegionTechniquePointerRCSpecialization.tpp"

#endif //MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUEPOINTERRCSPECIALIZATION_H


//class var<T, MM<MM<RCSimple, S_1, S_2>, true> {
