//
// Created by Szczepaniec, Jan  on 9/25/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUERCSPECIALIZATION_H
#define MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUERCSPECIALIZATION_H

#include "../ModelSpecialization.h"
#include "../Utility.h"

template<typename T, typename RT>
class var<T, RegionTechnique<RT, RCSimple> > {
public:
    template<typename ...Params>
    var(var<RT, RCSimple> &, Params &&... params);

    var(const var &v);

    var(var &v);

    var(var &&v) = delete;

    ~var();

    T &operator()() const;

    static void *operator new(std::size_t, void *p);

    var<RT, RCSimple> external_region;
private:
    T *body;
};

template<typename T, typename RT1, typename RT2, typename MM>
class var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > > {
public:
    template<typename ...Params>
    var(var<RT1, RegionTechnique<RT2, MM>> &, Params &&... params);

    var(const var &v);

    var(var &v);

    var(var &&v) = delete;

    ~var();

    T &operator()() const;

    static void *operator new(std::size_t, void *p);

    var<typename innermost_impl<RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::type, RCSimple> external_region;
private:
    T *body;
};

#include "RegionTechniqueRCSpecialization.tpp"

#endif //MMTECHNIQUES_THESIS_SZCZJ_REGIONTECHNIQUERCSPECIALIZATION_H
