//
// Created by Szczepaniec, Jan  on 9/25/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "RegionTechniqueSpecialization.h"

template<typename T, typename RT, typename MM>
template<typename ...Params>
var<T, RegionTechnique<RT, MM>>::var(RT &_region, Params &&... params)
        : body(_region.template construct<T>(std::forward<Params>(params)...)) {
}

template<typename T, typename RT, typename MM>
var<T, RegionTechnique<RT, MM>>::~var() {
};

template<typename T, typename RT, typename MM>
T &var<T, RegionTechnique<RT, MM>>::operator()() const {
    return *body;
}

template<typename T, typename RT, typename MM>
void *var<T, RegionTechnique<RT, MM>>::operator new(std::size_t, void *p) {
    return p;
}
