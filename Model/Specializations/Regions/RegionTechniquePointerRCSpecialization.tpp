//
// Created by Szczepaniec, Jan on 23/07/2018Created by Szczepaniec, Jan
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "RegionTechniquePointerRCSpecialization.h"

template<typename T, typename RT1, typename RT2>
var<var<T, RegionTechnique<RT1, RCSimple>>, RegionTechnique<RT2, RCSimple> >::var(var<RT2, RCSimple> &_region,
                                                                                  var<T,
                                                                                          RegionTechnique<RT1,
                                                                                                  RCSimple>> &v)
        : external_region(_region), body(_region().template construct<var<T, RegionTechnique<RT1, RCSimple>>>(v)) {
    if (external_region().root == v.external_region().root) { // is a local pointer
        body->external_region.release();
    }
}

template<typename T, typename RT1, typename RT2>
var<var<T, RegionTechnique<RT1, RCSimple>>,
        RegionTechnique<RT2, RCSimple> >::var(var &v) // pointing to resource on the same region var is allocated
        : body(&v()), external_region(v.external_region) {
}

template<typename T, typename RT1, typename RT2>
var<var<T, RegionTechnique<RT1, RCSimple>>, RegionTechnique<RT2, RCSimple> >::~var() {
};

template<typename T, typename RT1, typename RT2>
var<T, RegionTechnique<RT1, RCSimple>> &var<var<T, RegionTechnique<RT1, RCSimple>>,
        RegionTechnique<RT2, RCSimple> >::operator()() {
    return *body;
}

template<typename T, typename RT1, typename RT2>
void *var<var<T, RegionTechnique<RT1, RCSimple>>, RegionTechnique<RT2, RCSimple> >::operator new(std::size_t, void *p) {
    return p;
}