//
// Created by Szczepaniec, Jan  on 9/25/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "RegionTechniqueRCSpecialization.h"

template<typename T, typename RT>
template<typename ...Params>
var<T, RegionTechnique<RT, RCSimple>>::var(var<RT, RCSimple> &_region, Params &&... params)
        : external_region(_region), body(_region().template construct<T>(std::forward<Params>(params)...)) {
}

template<typename T, typename RT>
var<T, RegionTechnique<RT, RCSimple>>::var(const var &v): body(v.body), external_region(v.external_region) {
};

template<typename T, typename RT>
var<T, RegionTechnique<RT, RCSimple>>::var(var &v): var(static_cast<var const &>(v)) {
};

template<typename T, typename RT>
var<T, RegionTechnique<RT, RCSimple>>::~var() {
};

template<typename T, typename RT>
T &var<T, RegionTechnique<RT, RCSimple>>::operator()() const {
    return *body;
}

template<typename T, typename RT>
void *var<T, RegionTechnique<RT, RCSimple>>::operator new(std::size_t, void *p) {
    return p;
}

// nested var impl
template<typename T, typename RT1, typename RT2, typename MM>
template<typename ...Params>
var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::var(var<RT1, RegionTechnique<RT2, MM>> &_region,
                                                              Params &&... params)
        : external_region(_region.external_region),
          body(_region().template construct<T>(std::forward<Params>(params)...)) {
}

template<typename T, typename RT1, typename RT2, typename MM>
var<T,
        RegionTechnique<RT1,
                RegionTechnique<RT2,
                        MM> > >::var(const var &v): body(v.body), external_region(v.external_region) {
};

template<typename T, typename RT1, typename RT2, typename MM>
var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::var(var &v): var(static_cast<var const &>(v)) {
};

template<typename T, typename RT1, typename RT2, typename MM>
var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::~var() {
};

template<typename T, typename RT1, typename RT2, typename MM>
T &var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::operator()() const {
    return *body;
}

template<typename T, typename RT1, typename RT2, typename MM>
void *var<T, RegionTechnique<RT1, RegionTechnique<RT2, MM> > >::operator new(std::size_t, void *p) {
    return p;
}
