//
// Created by Szczepaniec, Jan on 9/5/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <iostream>
#include <list>
#include <array>
#include "../../Specializations/ReferenceCounting/RCSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"

namespace objc_autorelease {
    void copy2(var<int, RCSimple> c) {
        std::cout << "c is " << c() << std::endl;
    }

    void copy1(var<int, RCSimple> b) {
        copy2(b());
        std::cout << "b is " << b() << std::endl;
    }

    template<size_t K>
    var<decltype(std::list<std::string>(K)), RCSimple> createArray(var<std::string, RCSimple> &fileContents) {
        if (K == 1) {
            // int [N]
            var<decltype(std::list<std::string>(K)), RCSimple> urls(std::initializer_list<std::string>{fileContents()});
            return urls;
        }
        if (K == 2) {
            var<decltype(std::list<std::string>(K)), RCSimple>
                    urls(std::initializer_list<std::string>{fileContents(), fileContents()});
            return urls;
        }
        if (K == 3) {
            var<decltype(std::list<std::string>(K)), RCSimple>
                    urls(std::initializer_list<std::string>({fileContents(), fileContents(), fileContents()}));
            return urls;
        }
        return var<decltype(std::list<std::string>(K)), RCSimple>();
    }

//
    template<size_t K>
    void printList(var<decltype(std::list<std::string>(K)), RCSimple> s) {
        std::cout << "Start printing generated words:" << std::endl;
        for (std::string s2: s()) {
            std::cout << s2 << std::endl;
        }
        std::cout << "Finished printing generated words:" << std::endl;
    }

    void objc_autorelease() {
        var<int, RCSimple> a(30);

        std::cout << "a is " << a() << std::endl;

        copy1(a());

        var<std::array<int, 6>, RCSimple> nums_vec(std::array<int, 6>{1, 3, 2, 2, 3, 1});

        for (int s: nums_vec()) {
            { // autorelease
                var<std::string, RCSimple> fileContents(
                        "aaaaaaadsnfjansldjfnalsjdfnwljaenklasjndlsadjfnlaksdnfaakjdlnsaaaa");

                if (s == 1) {
                    var<std::list<std::string>, RCSimple> S(createArray<1>(fileContents));
                    printList<1>(S);
                }
                if (s == 2) {
                    var<std::list<std::string>, RCSimple> S(createArray<2>(fileContents));
                    printList<2>(S);
                }
                if (s == 3) {
                    var<decltype(std::list<std::string>(s)), RCSimple> S(createArray<3>(fileContents));
                    printList<3>(S);
                }
            }
        }
    }
}