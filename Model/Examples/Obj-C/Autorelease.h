//gcc 5.0.4

#import <Foundation/Foundation.h>

void copy2(int a) {
    NSLog(@"a is %d", a);
}

void copy1(int a) {
    copy2(a);
    NSLog(@"a is %d", a);
}

NSArray *createArray(int i, NSString *fileContents) {
    NSArray *urls =
            [[NSArray alloc] initWithObjects:[[NSString alloc] initWithString:fileContents], [[NSString alloc] initWithString:fileContents], [[NSString alloc] initWithString:fileContents], [[NSString alloc] initWithString:fileContents], [[NSString alloc] initWithString:fileContents], [[NSString alloc] initWithString:fileContents], nil];
    return urls;
}

int main(int argc, const char *argv[]) {

    int a = 30;
    NSLog(@"a is %d", a);

    copy1(a);
    NSArray *nums_vec = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:1], nil];

    for (NSNumber *i in nums_vec) {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        NSString *fileContents =
                [[NSString alloc] initWithString:@"aaaaaaadsnfjansldjfnalsjdfnwljaenklasjndlsadjfnlaksdnfaakjdlnsaaaa"];

        NSArray *S = createArray([i value], fileContents);

        for (NSString *s in S) {
            NSLog(@"New s is %@", s);
        }
        [pool drain];
    }
    return 0;
}

