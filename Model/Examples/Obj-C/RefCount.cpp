//
// Created by Szczepaniec, Jan on 10/23/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/ReferenceCounting/RCSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"

namespace objc_refcount {
    struct NSNumber {
        NSNumber(float _value) : value(_value) {}

        float value;
    };

    void objc_refcount() {
        var<NSNumber, RCSimple> value1(8.75);
        var<NSNumber, RCSimple> temp_value2(14.78);
        var<NSNumber, RCSimple> value2 = temp_value2;
        temp_value2.release();
        std::cout << "The first value is " << value1().value << " and the second value is " << value2().value
                  << std::endl;
    }
}