//
// Created by Szczepaniec, Jan on 10/09/2018.
//

#import <Foundation/Foundation.h>

int main(int argc, const char *argv[]) {
    NSNumber *value1 = [[NSNumber alloc] initWithFloat:8.75];
    NSNumber *temp_value2 = [[NSNumber alloc] initWithFloat:14.78];
    NSNumber *value2 = temp_value2;
    [temp_value2 release];
    NSLog(@"value1 is %f and value2 is %f ", [value1 floatValue], [value2 floatValue]);
}

//Output
// 2018-09-10 01:12:37.696 Main[10] value1 is 8.750000 and value2 is 14.780000