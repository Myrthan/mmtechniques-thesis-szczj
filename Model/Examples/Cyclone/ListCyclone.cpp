//
// Created by Szczepaniec, Jan on 9/6/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace cyclone_list {
    template<typename A, typename R>
    struct CycloneList {
        A hd;
        struct List<A, R> *tl;
    };

    typedef struct List<`a,`r> *`
    r list_t<
    `a,`r>;

    void cyclone_list() {

    }

// return a fresh copy of the list in r2
    list_t<`a,`r2>
    rcopy(region_t<
    `r2> r2, list_t<`a> x) {
    list_t result, prev;

    if (x == NULL) return
    NULL;
    result = rnew(r2)
    List {
    .
    hd = x->hd,
    .
    tl = NULL
};
prev = result;
for (
x = x->tl;
x !=
NULL;
x = x->tl
) {
prev->
tl = rnew(r2)
List(x
->hd,NULL);
prev = prev->tl;
]
}
return
result;
}

list_t<`a>
copy(list_t<
`a> x) {
return
rcopy(heap_region, x
);
}

// Return the length of a list.
int length(list_t
x) {
int i = 0;
while (x != NULL) {
++
i;
x = x->tl;
}
return
i;
}
}