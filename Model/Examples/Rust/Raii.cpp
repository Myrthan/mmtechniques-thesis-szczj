//
// Created by Szczepaniec, Jan on 9/5/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace rust_raii {
    using region = RegionTechnique<StaticRegion<>, StackAllocator>;

// This function takes ownership of a box and destroys it
    void create_box() {
        // START PRE METHOD CODE
        var<StaticRegion<>, StackAllocator> r;
        // END PRE METHOD CODE

        var<var<int, HeapAllocSBRM>, region> _box1(r(), 3);

        // `_box1` is destroyed here, and memory gets freed
    }

    void rust_raii() {
        // START PRE METHOD CODE
        var<StaticRegion<>, StackAllocator> r;
        // END PRE METHOD CODE

        var<var<int, HeapAllocSBRM>, region> _box2(r(), 5);

        // A nested scope:
        {
            var<region, StackAllocator> r1;
            var<var<int, HeapAllocSBRM>, region> _box3(r(), 4);

            // `_box3` is destroyed here, and memory gets freed
        }

        // Creating lots of boxes just for fun
        // There's no need to manually free memory!
        for (int i = 0; i < 1000; i++) {
            create_box();
        }

        // `_box2` is destroyed here, and memory gets freed
    }
}