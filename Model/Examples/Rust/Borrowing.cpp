//
// Created by Szczepaniec, Jan on 8/31/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace rust_borrowing {
    using region = RegionTechnique<StaticRegion<>, StackAllocator>;

// This function takes ownership of a box and destroys it
    template<typename MM1, typename MM2>
    void eat_box_i32(var<var<int, MM2>, MM1> &boxed_i32_temp) {
        // START PRE METHOD CODE
        var<StaticRegion<>, StackAllocator> r;
        // END PRE METHOD CODE

        var<var<int, HeapAllocSBRM>, region> boxed_i32(r(), std::move(boxed_i32_temp()));

        std::cout << "Destroying a box that contains " << (boxed_i32())() << std::endl;
    }

// This function borrows an i32
    template<typename MM>
    void borrow_i32(var<int, MM> &borrowed_i32) {
        std::cout << "This int is: " << borrowed_i32() << std::endl;
    }

    void rust_borrowing() {
        // START PRE METHOD CODE
        var<StaticRegion<>, StackAllocator> r;
        // END PRE METHOD CODE


        // Create a boxed i32, and a stacked i32
        var<var<int, HeapAllocSBRM>, region> boxed_i32(r(), 5);
        var<int, region> stacked_i32(r(), 6);

        // Borrow the contents of the box. Ownership is not taken,
        // so the contents can be borrowed again.
        borrow_i32((boxed_i32()));
        borrow_i32(stacked_i32);

        {
            // START PRE SCOPE CODE
            var<StaticRegion<>, StackAllocator> r2;
            // END PRE SCOPE CODE

            // Take a reference to the data contained inside the box
            var<int *, region> _ref_to_i3(r2(), &(boxed_i32()()));
            // Error!
            // Can't destroy `boxed_i32` while the inner value is borrowed.
            // eat_box_i32(boxed_i32);

            // `_ref_to_i32` goes out of scope and is no longer borrowed.
        }

        // `boxed_i32` can now give up ownership to `eat_box` and be destroyed
        eat_box_i32(boxed_i32);
    }
}