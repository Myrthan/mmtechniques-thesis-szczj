//
// Created by Szczepaniec, Jan on 9/4/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace rust_mutability {

    using region = RegionTechnique<StaticRegion<>, StackAllocator>;

    void mutability() {
        var<StaticRegion<>, StackAllocator> r;

        // Create a immutable_box
        var<var<int, HeapAllocSBRM>, region> immutable_box(r(), 5);

        std::cout << "immutable_box contains " << (immutable_box())() << std::endl;

        // Mutability error
        //*immutable_box = 4; // if we want we can have two implementation one with const one without

        // *Move* the box, changing the ownership (and mutability)
        var<var<int, HeapAllocSBRM>, region> mutable_box(r(), std::move(immutable_box()));

        std::cout << "mutable_box contains " << (mutable_box())() << std::endl;

        // Modify the contents of the box
        (mutable_box())() = 4;

        std::cout << "mutable_box now contains " << (mutable_box())() << std::endl;
    }
}