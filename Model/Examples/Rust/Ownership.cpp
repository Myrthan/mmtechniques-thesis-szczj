//
// Created by Szczepaniec, Jan on 8/20/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace rust_ownership {
    using region = RegionTechnique<StaticRegion<>, StackAllocator>;

    template<typename T, typename MM>
    void destroy_box(var<var<T, MM>, region> &ct) {
        // START PRE METHOD CODE
        var<StaticRegion<>, StackAllocator> r;

        var<var<int, HeapAllocSBRM>, region> c(r(), std::move(ct()));
        // END PRE METHOD CODE

        std::cout << "Destroying a box that contains " << (c())() << std::endl;

        // `c` is destroyed and the memory freed
    }

    void rust_ownership() {
        var<StaticRegion<>, StackAllocator> r;

        // _Stack_ allocated integer
        var<int, region> x(r(), 5);

        // *Copy* `x` into `y` - no resources are moved
        var<int, region> y(r(), x());

        // Both values can be independently used
        std::cout << "x is " << x() << ", and y is " << y() << std::endl;

        // `a` is a pointer to a _heap_ allocated integer
        var<var<int, HeapAllocSBRM>, region> a(r(), 4);

        std::cout << "a contains: " << (a())() << std::endl;

        // The pointer address of `a` is copied (not the data) into `b`.
        // Both are now pointers to the same heap allocated data, but
        // `b` now owns it.
        var<var<int, HeapAllocSBRM>, region> b(r(), std::move(a()));

        std::cout << "b contains: " << (b())() << std::endl;

        // Error! `a` can no longer access the data, because it no longer owns the

        // heap memory
        // std::cout << "a contains: "<< *(*a())() << std::endl;
        // Try uncommenting this line

        // This function takes ownership of the heap allocated memory from `b`
        destroy_box(b);

        // Since the heap memory has been freed at this point, this action would
        // result in dereferencing freed memory, but it's forbidden by the compiler
        // Error! Same reason as the previous Error
        //std::cout << "b contains: "<< *b() <<std::endl;
        // TODO ^ Try uncommenting this line
    }
}