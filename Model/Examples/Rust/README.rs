PRENOTE
To run rust example install Rust + valgrind
RUST installtion curl https://sh.rustup.rs -sSf | sh
command to run and check example raii.rs file
rustc raii.rs && valgrind ./raii

RUST Rules
TYPES: (shortcut is T)
Rust has two kind of types: scalar and compound.

Syntax:
C++ conversion: language type

Scalar types:
int: i8, u8, i16, u16, i32, u32, i64, u64, isize, usize
float/double: f32, f64
true, false: true, false
char: 'a' (Unicode Scalar Values)

Compound types:
tuple: (500, 6.4, 1)
arrays: [1, 2, 3, 4, 5]
        Example:
            let array: [i32; 3] = [1, 2, 3];
            let vector: Vec<i32> = vec![1, 2, 3];
const vector<Object>  : &[T] (slice, view on array)
        Example:
            let slice: &[i32] = &vector[..];
...
type_traits:Traits

STACK:



HEAP:
Box<T>
An allocation in the heap is guaranteed to reside at a single location in the heap for the whole lifetime of the allocation
- it will never be relocated as a result of moving a box value.
Operators:
T* Box::new(T)
