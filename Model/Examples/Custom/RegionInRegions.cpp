//
// Created by Szczepaniec, Jan on 15/08/2018.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/Regions/RegionTechniquePointerSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/ReferenceCounting/RCSpecialization.h"

namespace region_in_region {
    void region_in_region() {
        var<StaticRegion<2000>, RCSimple> r10;
        var<StaticRegion<1500>, RegionTechnique<StaticRegion<2000>, RCSimple>> r11(r10);
        var<StaticRegion<700>, RegionTechnique<StaticRegion<1500>, RegionTechnique<StaticRegion<2000>, RCSimple>>> r12(
                r11);
        var<StaticRegion<160>,
                RegionTechnique<StaticRegion<700>,
                        RegionTechnique<StaticRegion<1500>, RegionTechnique<StaticRegion<2000>, RCSimple>>> > r13(r12);
        var<StaticRegion<5>,
                RegionTechnique<StaticRegion<160>,
                        RegionTechnique<StaticRegion<700>,
                                RegionTechnique<StaticRegion<1500>,
                                        RegionTechnique<StaticRegion<2000>, RCSimple>>>>> r14(r13);
    }
}