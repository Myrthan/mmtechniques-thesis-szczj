//
// Created by Szczepaniec, Jan on 27/07/2018.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/Regions/RegionTechniquePointerSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/ReferenceCounting/RCSpecialization.h"

namespace multiple_region_pointer {

    using region = StaticRegion<>; // sizeof(struct F)
    using region_technique_rc = RegionTechnique<StaticRegion<>, RCSimple>; // sizeof(struct F)
    using region_technique_sbrm = RegionTechnique<StaticRegion<>, HeapAllocSBRM>; // sizeof(struct F)
    using var_region_sbrm = var<region, HeapAllocSBRM>;
    using var_region_rc = var<region, RCSimple>;
    using var_int_rc= var<int, RCSimple>;

    void multiple_region_pointer() {
        // R4->R3->R2->R1
        var_region_rc r1;
        var_region_rc r2;
        var_region_rc r3;
        var_region_sbrm r4;
        var<int, region_technique_rc> a(r1, -17);
//  var<var<int, region_technique_rc>, region_technique_rc> pointerToA(r2, r1, a);
//  var<var<var<int, region_technique_rc>, region_technique_rc>, region_technique_rc> pointerToPointerToA(r3, r2, pointerToA);
//  var<var<var<var<int, region_technique_rc>, region_technique_rc>, region_technique_rc>, region_technique_sbrm>
//      pointerToPointerToPointerToA(r4(), r3, pointerToPointerToA);
    }
}