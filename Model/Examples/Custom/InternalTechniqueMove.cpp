//
// Created by Szczepaniec, Jan on 9/17/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/ReferenceCounting/RCSpecialization.h"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"

namespace custom_techniques_internal_move {
    void custom_techniques_internal_move() {
        var<std::string, HeapAllocSBRM> a("moved_data (HeapAllocSBRM to RCSimple)");
        std::cout << "a is " << a() << std::endl;
        var<std::string, RCSimple> b(std::move(a));
        std::cout << "b is " << b() << std::endl;
        // reverse order
        var<std::string, RCSimple> c("moved_data (RCSimple to HeapAllocSBRM)");
        std::cout << "c is "
                  << c() << std::endl;
        var<std::string, HeapAllocSBRM> d(std::move(c));
        std::cout << "b is " << d() << std::endl;
    };
}