//
// Created by Szczepaniec, Jan on 30/04/2018.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/Regions/RegionTechniquePointerSpecialization.h"
#include "../../Specializations/Regions/RegionTechniquePointerRCSpecialization.h"

namespace mlkit_out_region_reference_simple {

    using static_region = StaticRegion<>; // sizeof(struct F)
    using region_rc_tech= RegionTechnique<static_region, RCSimple>; // sizeof(struct F)
    using region_static_tech = RegionTechnique<static_region, StackAllocator>;
    using var_region_local_type = var<static_region, StackAllocator>;
    using var_region_rc_type = var<static_region, RCSimple>;
    using var_rc_type = var<int, RCSimple>;

    var<var<bool, region_rc_tech>, region_static_tech> in_region_reference(var_region_local_type &r1) {
        var_region_rc_type r2;
        var<bool, region_rc_tech> b(r2, true);
        return var<var<bool, region_rc_tech>, region_static_tech>(r1(), b);
    }

    void out_region_reference_simple() {
        var_region_local_type r1;
        // pointer from external region example
        {
            var<var<bool, region_rc_tech>, region_static_tech> pointer_a(
                    [&r1]() -> var<var<bool, region_rc_tech>, region_static_tech> {
                        var_region_rc_type r2;
                        var<bool, region_rc_tech> a(r2, true);
                        std::cout << "a=" << a() << std::endl;
                        return var<var<bool, region_rc_tech>, region_static_tech>(r1(), a);
                    }());
            std::cout << "pointer to a is " << pointer_a()() << std::endl;
        }
        {
            // function in region reference, similar to example above
            auto pointer_b(in_region_reference(r1));
            std::cout << "pointer to variable b in in_region_reference is "
                      << (pointer_b()() ? "available" : "not available")
                      << std::endl;
        }

        // local pointer example
        {
            var_region_rc_type r3;
            var<int, region_rc_tech> c(r3, 30);
            var<var<int, region_rc_tech>, region_rc_tech> pointer_c(r3, c);
            std::cout << "c=" << c() << " pointer to c is " << pointer_c()() << std::endl;
        }
        // pointer_c is not available here
    };
}