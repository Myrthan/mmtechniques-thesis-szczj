let fun @ at r1 [r7:INF] (var255-0, var255-1)=
        (case var255-0
           of nil => var255-1
           |  _ =>
              let val ys = var255-1;
                  val xs = #1 decon_:: var255-0;
                  val x = #0 decon_:: var255-0
              in  :: (x, @[r7] <xs, ys>) at r7
              end
        ) (*case*) ;
    val l =
        letregion r19:1
        in @[r1]
           <:: (1, nil) at r19,
            :: (2, :: (3, nil) at r1) at r1
           >
        end (*r19:1*)
in  {|l: _, @: (_,r1)|}
end