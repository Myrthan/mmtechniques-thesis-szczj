Whenever a region variable occurs free in the effect
of an expression but occurs free neither in the
region-annotated type with place of the expression
nor in the type of any program variable that occurs
free in the expression then that region variable denotes
a region that is used only locally within the expression.
That this is true is of course far from trivial, but it
has been proved for a skeletal version of RegionExp.
Consequently, when this condition is met, the region
inference algorithm wraps a letregion binding
of the region variable around that expression.