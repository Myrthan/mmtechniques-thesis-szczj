val t = letreg p_0
        in let x = true @ p_0
           in \y.if x ! p_0 then y
                 else false @ p_1
           end
        end;
<-

val t = let x = true
		in (\y.if x then y else false)
        end;
