let val n =
    letregion r7:1
        in let val pair =
               (case true
                  of true => (3 + 4, 4 + 5) at r7
                | false => (4, 5) at r7
               ) (*case*)
        in  #0 pair
    end
end
    in  {|n: _|}
end

<-

val n = let
  val pair = if true then (3+4, 4+5)
             else (4, 5)
in
    #1 pair
end;