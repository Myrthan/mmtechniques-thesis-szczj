let val it =
        letregion r10:INF
        in let val l =
                   ::
                   (1,
                    :: (2, :: (3, nil) at r10) at r10
                   ) at r10
           in  (case l
                  of :: => #0 decon_:: l | _ => raise Bind
               ) (*case*)
           end
        end (*r10:INF*)
in  {|it: _|}
end

let val l = [1, 2, 3];
       val (x::_) = l
in x end;