//
// Created by Szczepaniec, Jan on 9/7/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "unordered_map"
#include "../../Specializations/ScopeBoundResourceManagement/HeapAllocSBRMSpecialization.h"
#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace mlkit_list {
    using region_static = StaticRegion<>;
    using region_dynamic = DynamicRegion<>;
    using static_region_technique_type = RegionTechnique<region_static, StackAllocator>;
    using dynamic_region_technique_type = RegionTechnique<region_dynamic, StackAllocator>;
    using var_region_static_type = var<region_static, StackAllocator>;
    using var_region_dynamic_type = var<region_dynamic, StackAllocator>;

    template<typename T>
    struct ListNode {
        ListNode(T _v) : v(_v), next(nullptr), prev(nullptr) {}

        T v;
        ListNode *next, *prev;
    };

    template<typename T>
    struct List {
        List() : front(nullptr), back(nullptr) {}

        ~List() {
            ListNode<T> *cur = front;
            while (cur) {
                ListNode<T> *toDeconstruct = cur;
                cur = cur->next;
                toDeconstruct->~ListNode();
            }
        }

        template<typename RT, typename MM>
        void push_front(var<RT, MM> &r, T e) {
            var<ListNode<T>, RegionTechnique<RT, MM>> node(r(), e);
            if (front == nullptr) {
                back = &node();
            }
            if (front != nullptr) {
                node().prev = front->prev;
                front->prev = &node();
                node().next = front;
            }
            front = &node();
        }

        ListNode<T> *front, *back;
    };

    template<typename RT, typename MM, typename MM1>
    var<List<int>, MM1> &addElement(var<RT, MM> &r10, int e, var<List<int>, MM1> &l) {
        l().push_front(r10, e);
        return l;
    }

    template<typename RT, typename MM>
    int getFirst(var<RT, MM> &r10) {
        // let
        //    let
        var<List<int>, RegionTechnique<RT, MM> > l(r10());
        //    in
        addElement(r10, 1, addElement(r10, 2, addElement(r10, 3, l)));
        // in
        var<int, StackAllocator> x = l().front->v; // val x
        return x();
        // end
    }

    int letregion() {
        // letregion
        var_region_dynamic_type var_r10;
        // in
        var<int, StackAllocator> i = getFirst(var_r10);
        // single word variable like int are not allocated within regions
        return i();
        // end
    }

    void mlkit_append() {
        var<int, StackAllocator> x = letregion();
        std::cout << "x is " << x() << std::endl;
    }
}