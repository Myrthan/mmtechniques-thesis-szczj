//
// Created by Szczepaniec, Jan on 11/5/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../../Specializations/Regions/RegionTechniqueSpecialization.h"
#include "../../Specializations/Struct/FieldSpecialization.h"
#include "../../Specializations/Regions/RegionTechniquePointerSpecialization.h"
#include "../../Specializations/StackAllocator/StackAllocatorSpecialization.h"

namespace mlkit_out_region_reference_lambda {

    using rc_alloc_region = RegionTechnique<StaticRegion<>, RCSimple>; // sizeof(struct F)
    using region_rc_type = var<StaticRegion<>, RCSimple>;
    using stack_alloc_region = RegionTechnique<StaticRegion<>, StackAllocator>; // sizeof(struct F)

    struct f {
        var<bool, rc_alloc_region> x;

        f(var<bool, rc_alloc_region> &_x) : x(_x) {};

        bool operator()(var<bool, StackAllocator> &y) {
            if (x()) {
                return y();
            } else {
                return false;
            }
        }
    };

    // @formatter:off
    // t = letreg p_0 in let x = true @ p_0 in \y.if x ! p_0 then y else false @ p_1
    void out_region_reference_lambda() {
        //letreg
            var<StaticRegion<>, StackAllocator> p_1;
        // in
        //      letreg
                    region_rc_type p_0;
        //      in
        //          let
                        var<bool, rc_alloc_region> x(p_0, true);
        //          in
                        var<f, stack_alloc_region> lambda(p_1(), x);
        //          end
        //      end
            var<bool, StackAllocator> y(true);
            std::cout << ((lambda()(y)) ? "True" : "False") << std::endl;
        //end
    }
    // @formatter:on
}