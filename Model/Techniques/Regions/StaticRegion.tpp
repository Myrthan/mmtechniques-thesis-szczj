//
// Created by Szczepaniec, Jan  on 8/30/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "StaticRegion.h"
#include "Region.h"

template<std::size_t buffer_bytes, std::size_t alignment>
StaticRegion<buffer_bytes, alignment>::StaticRegion() {
}

template<std::size_t buffer_bytes, std::size_t alignment>
template<typename type_storage>
StaticRegion<buffer_bytes, alignment>::StaticRegion(Region<type_storage> &_parent): Region<block_type>(_parent) {
}

template<std::size_t buffer_bytes, std::size_t alignment>
StaticRegion<buffer_bytes, alignment>::~StaticRegion() {
}

template<std::size_t buffer_bytes, std::size_t alignment>
template<typename T, typename ...Params>
T *StaticRegion<buffer_bytes, alignment>::construct(Params &&... params) {
    char *m_pointer = this->blocks.template allocate<sizeof(T), alignof(T)>();
    assert(m_pointer != nullptr && "Block has not enough free space to allocate given type.");
    T *t = new(m_pointer) T(std::forward<Params>(params)...);
    auto f = [t]() -> void { t->~T(); };
    this->destructor_callbacks.push(f);
    return t;
}
