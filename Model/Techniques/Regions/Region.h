//
// Created by Szczepaniec, Jan  on 8/26/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_REGION_H
#define MMTECHNIQUES_THESIS_SZCZJ_REGION_H

#include <cstddef>
#include "stack"
#include "../MemoryBlock.h"

/*
 * RT #1 template parameter is type of the region itself (dynamic, static)
 * MM #2 template parameter is type technique used for managing region
 */
template<typename RT, typename MM>
class RegionTechnique {
};

template<typename storage_type>
class Region {
public:
    Region() : root(this) {}

    Region(Region *_parent) : root(_parent->root) {
    }

    virtual ~Region() {
        while (!destructor_callbacks.empty()) {
            destructor_callbacks.top()();
            destructor_callbacks.pop();
        }
    }

    void *root;
protected:
    // stacked callbacks with destructors for constructed&allocated types within region
    std::stack<std::function<void()>> destructor_callbacks;
    // storage_type of blocks
    storage_type blocks;
};

#endif //MMTECHNIQUES_THESIS_SZCZJ_REGION_H
