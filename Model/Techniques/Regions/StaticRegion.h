//
// Created by Szczepaniec, Jan  on 8/30/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef MMTECHNIQUES_THESIS_SZCZJ_STATICREGION_H
#define MMTECHNIQUES_THESIS_SZCZJ_STATICREGION_H

#include <list>
#include <iostream>
#include <functional>
#include <cstddef>
#include "Region.h"

template<std::size_t buffer_bytes = 1000, std::size_t alignment = alignof(std::max_align_t)>
class StaticRegion : public Region<MemoryBlock<buffer_bytes, alignment>> {
    using block_type = MemoryBlock<buffer_bytes, alignment>;
public:
    StaticRegion();

    template<typename type_storage>
    StaticRegion(Region<type_storage> &_parent);

    ~StaticRegion();

    StaticRegion(const StaticRegion &) = delete;

    StaticRegion &operator=(const StaticRegion &) = delete;

    template<typename T, typename ...Params>
    T *construct(Params &&... params);
};

#include "StaticRegion.tpp"

#endif //MMTECHNIQUES_THESIS_SZCZJ_REGIONSTACK_H
