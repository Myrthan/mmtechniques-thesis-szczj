//
// Created by Szczepaniec, Jan  on 9/7/17.
//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

template<std::size_t buffer_bytes, std::size_t alignment>
MemoryBlock<buffer_bytes, alignment>::MemoryBlock(): m_pointer(buffer), availableBytes(buffer_bytes) {
}

template<std::size_t buffer_bytes, std::size_t alignment>
MemoryBlock<buffer_bytes, alignment>::~MemoryBlock() {
}

template<std::size_t buffer_bytes, std::size_t alignment>
template<std::size_t no_bytes_to_allocate, std::size_t type_alignment>
char *MemoryBlock<buffer_bytes, alignment>::allocate() {
    static_assert(no_bytes_to_allocate + alignment <= buffer_bytes,
                  "Is not possible to allocate given size because block is too small");
    static_assert(type_alignment <= alignment, "Type alignment is too big for block alignment");
    assert(isPointerValid(reinterpret_cast<char *>(m_pointer)) && "Pointer is invalid, not pointing to block");
    if (std::align(type_alignment, no_bytes_to_allocate, m_pointer, availableBytes)) {
        char *aligned_m_pointer = (char *) m_pointer;
        m_pointer = reinterpret_cast<char *>(m_pointer) + no_bytes_to_allocate;
        availableBytes -= no_bytes_to_allocate;
        return aligned_m_pointer;
    }
    return nullptr;
}

template<std::size_t buffer_bytes, std::size_t alignment>
void MemoryBlock<buffer_bytes, alignment>::deallocate(char *pointer) {
    assert(isPointerValid(pointer) && "Pointer is invalid, not pointing to block");
    availableBytes += reinterpret_cast<char *>(m_pointer) - pointer;
    m_pointer = (void *) pointer;
}

template<std::size_t buffer_bytes, std::size_t alignment>
bool MemoryBlock<buffer_bytes, alignment>::isPointerValid(char *p) const {
    return buffer <= p && p <= buffer + buffer_bytes;
}