//
// Copyright (c) 2019 Jan Szczepaniec
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <iostream>
#include "Model/Examples/Rust/Ownership.cpp"
#include "Model/Examples/Rust/OwnershipDynamicAllocation.cpp"
#include "Model/Examples/Rust/Borrowing.cpp"
#include "Model/Examples/Rust/BorrowingNonRegion.cpp"
#include "Model/Examples/Rust/Mutabilitty.cpp"
#include "Model/Examples/Rust/Raii.cpp"
#include "Model/Examples/Obj-C/Autorelease.cpp"
#include "Model/Examples/Obj-C/RefCount.cpp"
#include "Model/Examples/MLKit/Pair.cpp"
#include "Model/Examples/MLKit/List.cpp"
#include "Model/Examples/Custom/InternalTechniqueMove.cpp"
#include "Model/Examples/MLKit/Functor.cpp"
#include "Model/Examples/Custom/Array.cpp"
#include "Model/Examples/MLKit/MergingTechniques.cpp"
#include "Model/Examples/MLKit/FunctorFieldTechnique.cpp"
#include "Model/Examples/StackAllocator/StackAllocator.cpp"
#include "Model/Examples/Custom/Manual.cpp"
#include "Model/Examples/Custom/MultipleRegionPointer.cpp"
#include "Model/Examples/Custom/RegionInRegions.cpp"

int main() {
    // RUST
//    rust_ownership::rust_ownership();
//    rust_ownership_dynamic_allocation::rust_ownership_dynamic_allocation();
//    rust_borrowing::rust_borrowing();
//    rust_borrowing_non_region::rust_borrowing_non_region();
//    rust_mutability::mutability();
//    rust_raii::rust_raii();
    // Objective-C
//    objc_autorelease::objc_autorelease();
//    objc_refcount::objc_refcount();
    // MLKit
//    mlkitpair::mlkit_pair();
//    mlkit_list::mlkit_append();
//    mlkit_out_region_reference_simple::out_region_reference_simple();
//    mlkit_out_region_reference_field::out_region_reference_field();
    mlkit_out_region_reference_lambda::out_region_reference_lambda();
    // Custom
//    custom_techniques_internal_move::custom_techniques_internal_move();
//    simple_stack_alloc::simple_stack_alloc();
//    manual_allocation::manual_allocation();
//    simple_array::simple_arrays();
//    region_in_region::region_in_region();
  return 0;
}
