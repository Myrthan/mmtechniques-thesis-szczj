Master thesis code
====

This is an repository for master thesis code. The thesis aims were to construct an implementable model, that would gather any known deterministic memory management techniques from various languages homogeneously.

Repository presents implemented model that handle few deterministic techniques together with examples from various programming languages. The examples came from real programming languages. All examples are translated into the language that the model used together with the usage of techniques implementations provided by the model.

---

Building
--------

The best way to build the repository is by using in the main project directory:

```bash
$ cmake .
```

To compile the project, enter:

```bash
$ make
```

Upon successful compilation, the target file 'mmtechniques_thesis_szczj' will be placed in the root directory.

Problems
--------

1. I got following problem during the project compilation: 
    ```bash
       $ fatal error: boost/range/irange.hpp:  fatal error: boost/range/irange.hpp:
    ```
 Solution: You should install boost library. When using Ubuntu system you install it via
 ```bash
    $  sudo apt-get install libboost-all-dev
 ```    
2. I got following problem during the project compilation: 
    ```bash
       $ Please set CMAKE_CXX_COMPILER to a valid compiler path or name.
    ```
 Solution: Your system lacks of g++ or it is not linked. The project requries all essential components of g++ compiler installed and linked . When using Ubuntu system you install all essentials packages via
 ```bash
    $  sudo apt-get update && sudo apt-get install build-essential
 ```    
